Exercices : 

A partir de la commande de listage détaillé du contenu des répertoires ls -l , de la commande wc et des redirections des entrées/sorties, écrire pour chaque question la ligne de commande permettant de :

	a. Ranger dans le fichier sortieSTD la liste détaillée des fichiers du répertoire courant. Le fichier sortieSTD est créé s'il n'existe pas, écrasé sinon.

		==> ls -l 1>sortieSTD


	b.  Ranger, dans le fichier sortieERR , les messages d'erreurs et afficher à l'écran la liste détaillée des fichiers du répertoire /malraux , sachant que /malraux n'existe pas.

		==> ls -l /malraux 2>sortieERR


	c.  Ranger dans le fichier sortieSTD la liste des fichiers du répertoire courant et dans le fichier sortie.ERR les messages d'erreur.
	
		==> ls -l 1>sortieSTD 2>sortieERR


	d.  Lister les fichiers des répertoires /etc , /bin et /malraux et ajouter toutes les sorties dans le fichier sortieALL .

		==> ls /etc  /bin  /malraux &>>sortieALL


	e. Créer le fichier f.rep qui contiendra la liste simple de tous fichiers (même les éventuels fichiers cachés) du répertoire courant.

		==> ls -al > f.rep

	
	f. Ajouter dans le fichier f.rep le nombre de fichiers contenu dans le répertoire courant.

		==> wc -l <f.rep >>f.rep


	g. à partir de la commande cat et des redirections des entrées/sorties, écrire pour chaque question la ligne de commande permettant de :
		1. Lire depuis le clavier, et écrire tout sur l'écran sans distinction entre ce qui est erreur ou non.
			==> cat

		2. Copier le contenu du fichier f1 dans le fichier f2.  
			==> cat f1 > f2

		3. Concaténer les fichiers f1 , f2 et f3 dans cet ordre dans le fichier f123.
			==> cat f1 f2 f3 > f123

		4. Concaténer les fichiers f1 , f2 et f3 dans l'ordre inverse dans le fichier f321.
			==> cat f3 f2 f1 > f321

		5. Ajouter le contenu du fichier f1 à celui du fichier f2.
			==> cat f1 >> f2

		6. Créer le contenu du fichier f1 par la saisie au clavier (terminer).
			==> cat > f1

		7. Ajouter au contenu du fichier f2 le contenu de la saisie au clavier.
			==> cat >> f2

