Corrigé des exercices pratiques "Gestion des droits d'accès"


Donner les lignes de commandes permettant de changer les permissions des fichiers et dossiers afin de répondre aux besoins suivants :

1- le fichier /home/etudiant/sortieSTD ne doit être lisible que par le propriétaire, et doit pouvoir être modifiable par le propriétaire et les membres du groupe associé uniquement, le reste du monde n'a aucun droit.

	chmod u=rx,g=w /home/etudiant/sortieSTD		ou 	chmod 620 /home/etudiant/sortieSTD



2- le fichier /home/etudiant/sortieERR doit pouvoir être lu et modifié par le propriétaire, sans qu'il puisse l'exécuter, et que tous les autres (les membres du groupe propriétaire et le reste du monde) puissent en faire l'exécution mais ne puissent pas le modifier.

	chmod u=rw,go=x /home/etudiant/sortieERR	ou	chmod 611 /home/etudiant/sortieERR



3- le contenu du répertoire /home/etudiant/rep3/ ne doit pouvoir être listé que par le propriétaire et par le groupe associé.

	chmod ug=rx, o-x /home/etudiant/rep3/		ou	chmod 550 /home/etudiant/rep3/
